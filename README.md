# VetInventory

This is a 3-tier application, the iOS app serves as the primary input for barcodes, the admin dashboard was built in angular and a Node server ties everything together with MongoDB and Firebase as databases.

## Installation 

This repo contains files for both `Android` and `iOS`. It is developed with React-Native which means you need `react-native` installed.

### iOS

Develop and build for Apple iPhones.

#### Prerequisites
  * Xcode
  * react-native
  * npm  

#### Steps
  1. Clone the repo
  2. Go into the directory with `cd VetInventory`
  3. Install all Javascript dependencies with `npm install`
  4. Go into the ios directory `cd ios`
  5. Install iOS dependencies with `pod install`
  6. Return to parent directory `cd ..`
  7. Run application for ios with `react-natve run-ios`

### Android

Haven't done Android set up yet.

#### Prerequisites
  * Android Studio
  * react-native

### Steps
  * Will update soon

---

## Build to phone

### iOS

Building the app to your iphone is done in one of two configurations, `Release` or `Debug` where `Debug` is the default. 

The Debug configuration allows for both live and hot reloading so that you don't have to recompile to view your changes.

In order to use the app in `Debug` mode a development server _must be_ running on your Mac.

#### Build for Debug Configuration
  1. Plug your device into your computer
  2. Open Xcode
  3. In the top right corner select build to your device

#### Build for Release Configuration
  1. Follow the instructions found [here](https://www.christianengvall.se/react-native-build-for-ios-app-store/) and **DO NOT** commit these changes.


/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 * @flow
 */
import React from 'react';
import {createStackNavigator, createAppContainer} from 'react-navigation';
import { 
  HomeScreen, 
  ScanScreen, 
  MenuScreen, 
  LoginScreen, 
  EndScanScreen,
  PersonalProductFormScreen,
  NewProductFormScreen, 
  InventoryScreen 
} from './screens/screens';
import firebase from 'react-native-firebase';

import Icon from 'react-native-vector-icons/MaterialIcons'

Icon.loadFont();


const MainNavigator = createStackNavigator(
  {
    Home: {screen: HomeScreen},
    Login: {screen: LoginScreen},
    Menu: {screen: MenuScreen},
    Inventory: {screen: InventoryScreen},
    Scan: {screen: ScanScreen},
    ProductForm: {screen: NewProductFormScreen},
    PersonalProductForm: {screen: PersonalProductFormScreen},
    EndScan: {screen: EndScanScreen}
  },
  { 
    /* Uncomment these lines to test individual screens. */
    //initialRouteName: 'PersonalProductForm', // -> Screen you wish to go to
    //initialRouteParams: {code: {data: 'PLC38593430'}, client: {id: 'DEDE', deviceHolder: 'museagram@gmail.com'}, mode: 'add'} // -> Params for that screen
    
    initialRouteName: firebase.auth().currentUser ? 'Menu' : 'Home',
  }
);

const AppContainer = createAppContainer(MainNavigator);

export default class App extends React.Component {
  render() {
    return <AppContainer />;
  }
}
import React from 'react';
import {StyleSheet} from 'react-native';

export {
  vetButton,
  Layout,
  vetText
}


const vetButton = StyleSheet.create({
  // Button Styles
  button: {
    paddingRight: '15%',
    paddingLeft: '15%',
    marginBottom: 0,
    backgroundColor: '#3E8FE5'
  },
  buttonTitleStyle: {
    paddingTop: 10,
    paddingBottom: 10
  }
});

const Layout = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
  },
  header: {
    position: 'absolute',
    top: '12%',
    textAlign: 'center',
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  actions: {
    position: 'absolute',
    bottom: '10%'
  }
});

const vetText = StyleSheet.create({
  code: {
    fontWeight: "600",
    fontSize: 40,
    letterSpacing: 2
  }
});
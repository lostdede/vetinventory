import React from 'react';
import {StyleSheet, Text, View, FlatList, Animated, StatusBar} from 'react-native';
import { ListItem } from 'react-native-elements';
import { Layout } from '../components/vet-styles';
import { DB } from './../lib/vet-lib';

export default class InventoryScreen extends React.Component {
  userModel = {
    name: '',
    config: {
      headerColor: 'transparent'
    }
  };

  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);
    this.state = {
      user: this.props.navigation.getParam('client', this.userModel),
      InventoryList: []
    }

    this.scaleValue = new Animated.Value(0);
  }

  componentDidMount() {
    let templist = []
    DB.getInventory(this.state.user.id).then(ref => ref.onSnapshot((snapshot) => {
      snapshot.docChanges.forEach((change) => {
        templist.push(change.doc.data());
        this.setState({InventoryList: templist});
      });
    }));

    Animated.timing(this.scaleValue, {
      toValue: 1,
      duration : 1000,
      delay: this.props.index * 350
    }).start();
  }

  render() {
    const {navigate} = this.props.navigation;
    let customHeaderStyles = {...styles.header};
    customHeaderStyles.backgroundColor = this.state.user.config.headerColor;
    let customTextColor = {color: (this.state.user.config.headerTextColor || '#333333')};

    return (
      <View style={Layout.container}>
      <View style={customHeaderStyles}>
        <Text style={[styles.headerText, customTextColor]}>{this.state.user.name}</Text>
        <StatusBar barStyle={this.state.user.config.statusBar} animated />
      </View>
      <Animated.View style={[styles.listBox, { opacity: this.scaleValue }]}>
        <FlatList data={this.state.InventoryList} renderItem={({item}) => (<ListItem key={item.id} containerStyle={styles.list} titleStyle={styles.listTitle} rightSubtitleStyle={styles.listSubtitle} title={item.name || item.id} rightSubtitle={item.quantity.toString()} />)} />
      </Animated.View>
    </View>
    )
  }
}

const styles = StyleSheet.create({
  header: {
    position: 'absolute',
    top: 0,
    backgroundColor: 'yellow',
    padding: 20,
    width: '100%',
    height: 100
  },
  headerText: {
    textAlign: 'center',
    marginTop: 40,
    fontSize: 15,
    fontWeight: "400"
  },
  listBox: {
    width: '85%',
    display: 'flex',
    flex: 1,
    top: 120,
    marginBottom: 160
  },
  list: {
    width: '100%',
    marginBottom: 25,
    borderRadius: 8,
    backgroundColor: '#FEFEFE'
  },
  listTitle: {
    padding: 20
  },
  listSubtitle: {
    padding: 10
  }
});
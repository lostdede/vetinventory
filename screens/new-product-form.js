import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Image} from 'react-native';
import { Button, Input, ButtonGroup } from 'react-native-elements';
import firebase from 'react-native-firebase';
import { Layout, vetButton, vetText } from '../components/vet-styles';


export default class NewProductFormScreen extends React.Component {
  static navigationOptions = {
    header: null
  };

  userModel = {
    name: '',
    config: {
      headerColor: 'transparent'
    }
  };

  codeModel = {
    data: 'None found',
    type: 'Supposed to be barcode',
    rawData: 'Raw data',
    bounds: 'Bounds'
  };

  constructor (props) {
    super(props);
    this.state = {
      client: this.props.navigation.getParam('client', this.userModel),
      code: this.props.navigation.getParam('code', this.codeModel),
      selectedIndex: 0,
      quantity: null
    };

    this.updateIndex = this.updateIndex.bind(this);
    this.continue = this.continue.bind(this);
  }

  updateIndex (selectedIndex) {
    this.setState({selectedIndex})
  }

  continue() {
    const {navigate} = this.props.navigation;
    const operation = this.state.selectedIndex == 0 ? 'add' : 'remove';
    navigate('EndScan', {code: this.state.code, client: this.state.client, mode: operation, quantity: this.state.quantity});
  }

  render() {
    const choiceButtons = ['Add', 'Remove'];
    const { selectedIndex } = this.state;

    return (
      <View style={Layout.container}>
        <View style={Layout.header}>
          <Text style={Layout.instructions}>You just scanned:</Text>
          <Text style={[vetText.code, styles.pad]}>{this.state.code.data}</Text>
          
          <View style={styles.selectionBox}>
            <Text style={Layout.instructions}>What would you like to do?</Text>
            <ButtonGroup
              onPress={this.updateIndex}
              selectedIndex={selectedIndex}
              buttons={choiceButtons}
              containerStyle={styles.choices}
              buttonStyle={styles.radio} 
            />
          </View>
        </View>
        <View style={styles.FormBox}>
          <Input inputContainerStyle={styles.FormInput} keyboardType="numeric" name="quantity" onChangeText={(txt) => this.setState({quantity: txt})} value={this.state.quantity} maxLength={4} label='Quantity' />
        </View>
        <View style={Layout.actions}>
          <Button disabled={!this.state.quantity} onPress={this.continue} titleStyle={vetButton.buttonTitleStyle} buttonStyle={vetButton.button} title="Submit"></Button>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  FormBox: {
    width: '80%'
  },
  choices: {
    borderRadius: 5,
    marginBottom: 100
  },
  FormInput: {
    borderRadius: 8,
  },
  radio: {

  },
  selectionBox: {
    marginTop: 50
  },
  pad: {
    marginTop: 15
  }
});
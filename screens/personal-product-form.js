import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Image} from 'react-native';
import { Button, Input, ButtonGroup } from 'react-native-elements';
import { VTMongo } from './../lib/mongo-service';
import DefaultPreference from 'react-native-default-preference';
import { Layout, vetButton, vetText } from '../components/vet-styles';
import { DB } from '../lib/fbdb';


export default class PersonalProductFormScreen extends React.Component {
  static navigationOptions = {
    header: null
  };

  userModel = {
    name: '',
    config: {
      headerColor: 'transparent'
    }
  };

  codeModel = {
    data: 'None found',
    type: 'Supposed to be barcode',
    rawData: 'Raw data',
    bounds: 'Bounds'
  };

  constructor (props) {
    super(props);
    this.state = {
      client: this.props.navigation.getParam('client', this.userModel),
      code: this.props.navigation.getParam('code', this.codeModel),
      name: '',
      brandName: '',
      quantity: null,
      error: 'edede'
    };

    this.continue = this.continue.bind(this);
    this.shouldProceed = this.shouldProceed.bind(this);
  }

  componentDidMount() {
    
  }

  async shouldProceed() {
    const {navigate} = this.props.navigation;
    const verdict = await DB.clientHasItem(this.state.code.data)
    .then(res => {
      res
    })
    .catch(err => err);

    if (verdict) {
      this.continue();
    }
  }

  continue() {
    const {navigate} = this.props.navigation;

    const item = {
      name: this.state.name,
      barcode: this.state.code.data,
      msrp: 'Not Provided',
      brandName: this.state.brandName,
      images: '',
      addedBy: `${this.state.client.id} | ${DefaultPreference.get('CompanyUserEmail')}`,
      addedAt: new Date(),
      updatedAt: new Date()
    }

    // Add the item to our personal product DB
    VTMongo.addItem(item, 'personal');

    navigate('EndScan', {code: this.state.code, client: this.state.client, quantity: this.state.quantity, isPersonal: true, isPersonalItem: item});
  }

  render() {
    this.shouldProceed();

    return (
      <View style={Layout.container}>
        <View style={Layout.header}>
          <Text style={Layout.instructions}>You just scanned: </Text>
          <Text style={[vetText.code, styles.pad]}>{this.state.code.data}</Text>
        </View>


        <View style={styles.FormBox}>
          <Input inputContainerStyle={styles.FormInput} name="name" onChangeText={(txt) => this.setState({name: txt})} value={this.state.name} label='Item Name' />
          <Input inputContainerStyle={styles.FormInput} name="name" onChangeText={(txt) => this.setState({brandName: txt})} value={this.state.brandName} label='Item Brand' />
          <Input inputContainerStyle={styles.FormInput} keyboardType="decimal-pad" returnKeyType='done' name="quantity" onChangeText={(txt) => this.setState({quantity: txt})} value={this.state.quantity} label='Quantity' />
        </View>
        <View style={Layout.actions}>
          <Button disabled={!this.state.quantity} onPress={this.continue} titleStyle={vetButton.buttonTitleStyle} buttonStyle={vetButton.button} title="Submit"></Button>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  FormBox: {
    width: '80%'
  },
  FormInput: {
    borderRadius: 8,
    margin: 8,
    marginBottom: 5
  },
  pad: {
    marginTop: 15
  }
});
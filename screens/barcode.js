/**
 * 
 * Barcode Scanner View
 *
 */

import React from 'react';
import {StyleSheet, Text, View, StatusBar} from 'react-native';
import { DB } from './../lib/vet-lib';

// Styling
import { Button } from 'react-native-elements';
import { RNCamera } from 'react-native-camera';


export default class ScanScreen extends React.Component {
  static navigationOptions = ({ navigation }) => {
    if (navigation.getParam('header', null)) {
      return {
        title: 'Scan Item',
        headerTransparent: true
      }
    } else {
      return {
        header: null
      }
    }
  };

  constructor (props) {
    super(props);
    this.state = { 
      client: this.props.navigation.getParam('client', null),
      mode: this.props.navigation.getParam('mode', 'add'),
      codes: [],
      readBarcode: null
    };

    this.shouldProceed = this.shouldProceed.bind(this);
    this.decisionPath = this.decisionPath.bind(this);

  }
  

  componentDidMount() {
    
  }

  async shouldProceed(barcodes) {
    const verdict = await DB.clientHasItem(this.state.client.id, barcodes.data)
    .then(res => res);

    return verdict;
  }
  
  async decisionPath(barcodes) {
    const {navigate} = this.props.navigation;
    const goToEnd = await this.shouldProceed(barcodes).then(res => res);

    if (this.state.mode === 'bulk') {
      navigate('ProductForm', {code: barcodes, client: this.state.client});
    } else if (this.state.client.plan === 'personal') {
    
      if (goToEnd) {
        navigate('EndScan', {code: barcodes, client: this.state.client});  
      } else {
        navigate('PersonalProductForm', {code: barcodes, client: this.state.client});
      }
    } else {
      navigate('EndScan', {code: barcodes, client: this.state.client});
    }
  }

  render() {

    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Text style={styles.instructions}>Place barcode in front of camera</Text>
          <StatusBar barStyle="default" animated />
        </View>
        <View style={styles.camera}>
          <RNCamera ref={ref => {this.camera = ref;}} style={styles.preview} type={RNCamera.Constants.Type.back} barCodeTypes={[RNCamera.Constants.BarCodeType.ean13, RNCamera.Constants.BarCodeType.ean8]} onBarCodeRead={(barcodes) => this.decisionPath(barcodes)}></RNCamera>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#F5FCFF',
    height: 100
  },
  header: {
    position: 'absolute',
    top: '12%',
    textAlign: 'center',
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  actions: {
    position: 'absolute',
    bottom: '10%'
  },
  button: {
    paddingRight: '15%',
    paddingLeft: '15%',
    marginBottom: 0
  },
  camera: {
    display: 'flex',
    width: '90%',
    height: '60%'
  },
  preview: {
    flex: 1,
    justifyContent: 'flex-end',
    alignItems: 'center'
  },
  capture: {
    flex: 0,
    backgroundColor: '#fff',
    borderRadius: 5,
    color: '#000',
    padding: 10,
    margin: 40
  }
});

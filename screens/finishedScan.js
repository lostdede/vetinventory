import React, {Component} from 'react';
import {StyleSheet, Text, View, Image} from 'react-native';
import { DB } from './../lib/vet-lib';

// Styling
import { Button } from 'react-native-elements';
import { Layout, vetButton } from './../components/vet-styles';

const copy = {
  add: 'The item has been added',
  remove: 'The item has been deleted',
  bulk: 'Items have been processed'
};

export default class ScanScreen extends React.Component {
  static navigationOptions = {
    header: null
  };

  constructor (props) {
    super(props);
    this.state = { 
      error: '',
      code: this.props.navigation.getParam('code', {data: 'None found'}),
      mode: this.props.navigation.getParam('mode', 'add'),
      isPersonal: this.props.navigation.getParam('isPersonal', false),
      isPersonalItem: this.props.navigation.getParam('isPersonalItem', {}),
      quantity: this.props.navigation.getParam('quantity', 1)
    };

    if (this.state.code.data !== 'None found') {
      const barcode = this.props.navigation.getParam('code').data;
      const client = this.props.navigation.getParam('client', null);
      
      if (client && !isNaN(barcode)) {
        if (this.state.mode == 'add' && this.state.isPersonal) {
          DB.addToPersonalInventory(client.id, client.deviceHolder, barcode, this.state.quantity, this.state.isPersonalItem);
        } else if (this.state.mode == 'add') {
          DB.addToInventory(client.id, client.deviceHolder, barcode, this.state.quantity);
        } else if (this.state.mode == 'remove') {
          DB.deleteFromInventory(client.id, barcode, this.state.quantity);
        }
      } else {
        this.setState({error: 'Something went wrong, code not scanned.'});
      }
    }
  }

  render() {
    const {navigate} = this.props.navigation;
    return (
      <View style={Layout.container}>
        <View style={Layout.header}>
          <Text style={Layout.instructions}>{ copy[this.state.mode] || this.state.error}</Text>
          <Text style={Layout.instructions}>Your code is:</Text>
        </View>
        <View style={styles.midBox}>
          <Image style={styles.logo} source={require('./../img/done.png')}></Image>
          <Text style={styles.code}>{this.state.code.data}</Text>
        </View>
        <View style={Layout.actions}>
          <Button buttonStyle={styles.button} title="New Item" type="solid" onPress={() => navigate('Scan')}/>
          <Button buttonStyle={styles.button} title="Menu" type="solid" onPress={() => navigate('Menu')}/>
        </View>
      </View>
      
    );
  }
}

const styles = StyleSheet.create({
  midBox: {
    position: 'absolute',
    top: 180,
    marginTop: 1,
    alignItems: 'center'
  },
  logo: {
    width: 60,
    height: 60,
    marginBottom: 40
  },
  code: {
    fontWeight: "600",
    fontSize: 40,
    letterSpacing: 2
  },
  button: {
    paddingRight: '15%',
    paddingLeft: '15%',
    marginBottom: 25,
  }
});
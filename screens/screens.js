import HomeScreen from './home';
import LoginScreen from './login';
import MenuScreen from './menu';
import InventoryScreen from './inventory';
import ScanScreen from './barcode';
import NewProductFormScreen from './new-product-form';
import PersonalProductFormScreen from './personal-product-form';
import EndScanScreen from './finishedScan';

export {
  HomeScreen,
  LoginScreen,
  MenuScreen,
  InventoryScreen,
  ScanScreen,
  PersonalProductFormScreen,
  NewProductFormScreen,
  EndScanScreen
}
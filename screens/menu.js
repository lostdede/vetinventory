import React from 'react';
import {Animated, StyleSheet, Text, View, ActivityIndicator, StatusBar} from 'react-native';
import { Button, Icon, Overlay } from 'react-native-elements';
import firebase from 'react-native-firebase';
import { Layout, vetButton } from '../components/vet-styles';
import { DB } from './../lib/vet-lib';
import DefaultPreference from 'react-native-default-preference';

export default class MenuScreen extends React.Component {
  userModel = {
    name: '',
    deviceHolder: '',
    config: {
      headerColor: 'transparent'
    }
  };

  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);
    this.state = {
      menuVisible: false,
      user: this.props.navigation.getParam('client', this.userModel),
      auth: this.props.navigation.getParam('fbuser', null) || firebase.auth().currentUser,
      loading: 'null'
    }

    this.fadeAnimation = new Animated.Value(0);
    this.signOut = this.signOut.bind(this);

    if (this.state.user === this.userModel) {
      this.state.loading = true;
    } else {
      this.state.loading = false;
    }
  }

  componentDidMount() {
    const { navigate } = this.props.navigation;
    if (this.state.user == this.userModel) {
      DB.getClient(this.state.auth.email, 'email').then((query) => {
        let userForState = query.docs[0].data();
        DefaultPreference.get('CompanyUserEmail')
        .then((val) => {
          userForState.deviceHolder = val;
          this.setState({user: userForState, loading: false});
        })
        .catch((error) => {
          this.signOut();
          navigate('Home');
        });
      });
    }

    Animated.timing(this.fadeAnimation, {
      toValue: 1,
      duration: 900,
      useNativeDriver: true,      
    }).start()
  }

  signOut() {
    const { navigate } = this.props.navigation;
    this.setState({ 
      menuVisible: false,
      user: this.userModel,
      auth: null,
      loading: 'null'
    });
    firebase.auth().signOut().then(() => {
      DB.companyUserLogout(this.state.user.id, this.state.user.deviceHolder);
      DefaultPreference.clear('CompanyUserEmail');
      navigate('Home');
    });
  }

  render() {
    const {navigate} = this.props.navigation;
    let customHeaderStyles = {...styles.header};
    customHeaderStyles.backgroundColor = this.state.user.config.headerColor;
    let customTextColor = {color: (this.state.user.config.headerTextColor || '#333333')};
    return (
      <View style={Layout.container}>
        {this.state.loading ?  
          <View styles={styles.loader}>
            <ActivityIndicator size="large"></ActivityIndicator>
          </View>
        : 
        <Animated.View style={[Layout.container, {opacity: this.fadeAnimation}]}>
          <View style={customHeaderStyles}>
            <Text style={[styles.headerText, customTextColor]}>{this.state.user.name}</Text>
            <Icon color={customTextColor.color} containerStyle={styles.settings} name="settings" onPress={() => this.setState({menuVisible: true})}/>
            <StatusBar animated barStyle={this.state.user.config.statusBar} showHideTransition='slide' />
          </View>
          <Text style={styles.prompt}>What would you like to do?</Text>
          <View style={styles.menuBox}>
            <Button titleStyle={styles.tileText} buttonStyle={styles.tile} onPress={() => navigate('Scan', {client: this.state.user, mode: 'add', header: true} )} title="Add a single item">
            </Button>
            <Button titleStyle={styles.tileText} buttonStyle={styles.tile} onPress={() => navigate('Scan', {client: this.state.user, mode: 'remover', header: true})} title="Remove a single item"></Button>
            <Button titleStyle={styles.tileText} buttonStyle={styles.tile} onPress={() => navigate('Scan', {client: this.state.user, mode: 'bulk', header: true})} title="Bulk scan"></Button>
          </View>
          <Overlay
            isVisible={this.state.menuVisible}
            windowBackgroundColor="rgba(0, 0, 0, .5)"
            overlayBackgroundColor="#fefefe"
            width="80%"
            height="50%"
            style={styles.overlay}
            onBackdropPress={() => this.setState({ menuVisible: false })}
          >
            <View>
              <Button titleStyle={vetButton.buttonTitleStyle} buttonStyle={[vetButton.button, styles.overlayActions]} onPress={() => {
                navigate('Inventory', {client: this.state.user});
                this.setState({ menuVisible: false });
                }} title="View Inventory"></Button>
              <Button buttonStyle={[vetButton.button, styles.overlayActions]} titleStyle={vetButton.buttonTitleStyle} title="Sign Out" onPress={this.signOut}></Button>
            </View>
          </Overlay>
        </Animated.View>
        }
      </View>
      
    )
  }
}

const styles = StyleSheet.create({
  overlay: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
    padding: 40
  },
  overlayActions: {
    margin: 10
  },
  loader: {
    width: 100,
    height: 100
  },
  header: {
    position: 'absolute',
    top: 0,
    backgroundColor: 'yellow',
    padding: 20,
    width: '100%',
    height: 100
  },
  headerText: {
    textAlign: 'center',
    marginTop: 40,
    fontSize: 15,
    fontWeight: "400"
  },
  settings: {
    position: 'absolute',
    right: 15,
    bottom: 20
  },
  prompt: {
    position: 'absolute',
    textAlign: 'center',
    color: '#333333',
    top: 150
  },
  menuBox: {
    width: '85%',
    marginTop: 120
  },
  tile: {
    width: '100%',
    marginBottom: 25,
    backgroundColor: '#3E8FE5',
    borderRadius: 8
  },
  tileText: {
    paddingTop: 35,
    paddingBottom: 35
  }
});
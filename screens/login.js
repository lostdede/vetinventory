import React from 'react';
import {Platform, StyleSheet, Text, View} from 'react-native';
import { Button, Input, Icon} from 'react-native-elements';
import firebase from 'react-native-firebase';
import {vetButton, Layout} from '../components/vet-styles';
import { DB } from '../lib/vet-lib';
import DefaultPreference from 'react-native-default-preference';
import { createCipher, createDecipher} from './../lib/crypto';
import { key } from './../lib/secure';

const password = key;

function encrypt(data) {
  const cipher = createCipher('aes-256-ecb', password);
  return cipher.update(data, 'utf8', 'hex') + cipher.final('hex');
}

export default class LoginScreen extends React.Component {
  static navigationOptions = {
    header: null
  };

  constructor (props) {
    super(props);
    this.state = {
      client: null,
      code: {
        one: '',
        two: '',
        three: '',
        four: ''
      },
      error: null,
      email: null,
      validForm: true,
      loading: false
    };
    this.handleChange = this.handleChange.bind(this);
    this.submitCode = this.submitCode.bind(this);
    this.login = this.login.bind(this);
    this.enableButton = this.enableButton.bind(this);
  }

  handleChange(txt, name) {
    let code = {...this.state.code};    
    code[name] = txt;
    this.setState({code});
  }

  // Use the four input fields to build our 4-digit code
  gatherCode() {
    codeString = this.state.code.one + this.state.code.two + this.state.code.three + this.state.code.four;
    if (codeString.length < 4) {
      this.setState({error: 'Please enter a valid code', validForm: false});
      // handle error
    }
    this.setState({validForm: false});
    return codeString;
  }

  // Sign into firebase using company email
  login(email) {
    const {navigate} = this.props.navigation;
    firebase.auth().signInWithEmailAndPassword(email, encrypt(email))
      .then((authmans) => {
        if (authmans) {
          this.setState({loading: false});
          navigate('Menu', {client: this.state.client, fbuser: authmans})
        } else {
          firebase.auth().signOut();
        }
      })
      .catch(err => this.setState({error: err}));
  }

  /* 
   * Verify the company code, retrieve the email for the company, sign in and 
   * verify the user is part of that company.
   */
  submitCode() {
    let code = this.gatherCode();
    this.setState({email: this.state.email, loading: true});
    DB.getClient(code).then((query) => {
      let gotClient = query.docs[0].data();
      let CleanEmail = this.state.email.trim().toLowerCase();
      gotClient.deviceHolder = CleanEmail;
      this.setState({client: gotClient});

      // Store company user email locally on phone
      DefaultPreference.set('CompanyUserEmail', CleanEmail);

      let userList = gotClient.users;

      let screenedUserList = userList.filter(x => (x.email == CleanEmail));

      if (screenedUserList.length == 0) {
        this.setState({
          client: null,
          code: {
            one: '',
            two: '',
            three: '',
            four: ''
          },
          error: 'The email you entered is not associated with this company.',
          email: null,
          validForm: true,
          loading: false
        });
      } else {
        let idx = userList.indexOf(screenedUserList[0]);
        userList.splice(idx, 1);
        screenedUserList[0].devices = screenedUserList[0].devices + 1;
        screenedUserList[0].platform = Platform.OS;
        screenedUserList[0].lastSignIn =  new Date().toString();
        userList.push(screenedUserList[0]);

        DB.companyUserLogin(query.docs[0].id, userList);
        this.login(this.state.client.email);
      }
    }).catch(error => this.setState({error: `That company code is incorrect: ${error}`, loading: false}));
  }

  // Check that all fields have been entered before enabling button
  enableButton() {
    let code = this.gatherCode();
    let email = this.state.email;

    var re = /^([a-zA-Z0-9_\-\.]+)@([a-zA-Z0-9_\-\.]+)\.([a-zA-Z]{2,5})$/; 

    if (!code ) {
      this.setState({validForm: true});
    } else if (!email) {
      this.setState({validForm: true});
    } else if (!re.test(email)) {
      this.setState({validForm: true});
    } else {
      this.setState({validForm: false});
    }
  }

  render() {
    return (
      <View style={Layout.container}>
        <Text style={Layout.header}>{this.state.error ? this.state.error : 'Please enter your company\'s code'}</Text>
        <View style={styles.LoginForm} >
          <Input selectionColor={'#888888'} autoFocus name="one" onChangeText={(txt) =>  {
             this.handleChange(txt, 'one');
             this.codeBox2.focus();
          }} value={this.state.code.one} maxLength={1} inputStyle={styles.LoginFormInputText} inputContainerStyle={styles.LoginFormInput}/>
          <Input ref={(r) => this.codeBox2 = r } selectionColor={'#888888'} name="two" onChangeText={(txt) =>  { 
            this.handleChange(txt, 'two');
            this.codeBox3.focus();
            }} value={this.state.code.two} maxLength={1} inputStyle={styles.LoginFormInputText} inputContainerStyle={styles.LoginFormInput}/>
          <Input ref={(r) => this.codeBox3 = r } selectionColor={'#888888'} name="three" onChangeText={(txt) =>  { 
            this.handleChange(txt, 'three');
            this.codeBox4.focus();
            }} value={this.state.code.three} maxLength={1} inputStyle={styles.LoginFormInputText} inputContainerStyle={styles.LoginFormInput}/>
          <Input ref={(r) => this.codeBox4 = r } selectionColor={'#888888'} name="four" onChangeText={(txt) =>  { 
            this.handleChange(txt, 'four')
            this.emailBox.focus();
        }} value={this.state.code.four} maxLength={1} inputStyle={styles.LoginFormInputText} inputContainerStyle={styles.LoginFormInput}/>
        </View>
        <View style={styles.LoginFormEmailBox}>
          <Input ref={(r) => this.emailBox = r } label="Email" placeholder="Enter your email" value={this.state.email} onChangeText={(txt) => {
            this.setState({email: txt});
            this.enableButton();
            }} keyboardType="email-address"/>
        </View>
        <View style={Layout.actions}>
          {this.state.loading ? <Button type="clear" titleStyle={vetButton.buttonTitleStyle} buttonStyle={vetButton.button} loading/> : <Button titleStyle={vetButton.buttonTitleStyle} buttonStyle={vetButton.button} title="Go" type="solid" disabled={this.state.validForm} onPress={() => this.submitCode()}/>}
        </View>
      </View>
    );
  }
}

// Component Specific Styling
const styles = StyleSheet.create({
  LoginForm: {
    flexDirection: 'row',
    width: 115,
    height: 100,
    position: 'absolute',
    textAlign: 'center',
    padding: 8,
    left: 2,
    top: '25%'
  },
  LoginFormInput: {
    borderRadius: 8,
    borderWidth: 2.5,
    borderColor: '#3E7FC3',
    backgroundColor: '#F8F8F8',
    width: 70,
    height: 90,
    margin: 5,
    borderBottomWidth: 2.5
  },
  LoginFormInputText: {
    textAlign: 'center',
    fontSize: 40,
    fontWeight: '800'
  },
  LoginFormEmailBox: {
    width: '80%'
  }
});
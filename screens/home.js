import React, {Component} from 'react';
import {Platform, StyleSheet, Text, View, Image} from 'react-native';
import { Button } from 'react-native-elements';
import firebase from 'react-native-firebase';


export default class HomeScreen extends React.Component {
  static navigationOptions = {
    header: null
  };

  constructor(props) {
    super(props);
    this.whereTo = this.whereTo.bind(this);
  }

  whereTo() {
    const {navigate} = this.props.navigation;
    if (firebase.auth().currentUser) {
      navigate('Menu');
    } else {
      navigate('Login');
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <Image style={styles.logo} source={require('./../img/logo.png')}></Image>
        <Text style={styles.welcome}>VetInventory</Text>
        <View style={styles.actions}>
          <Button titleStyle={styles.buttonTitleStyle} buttonStyle={styles.button} title="Start" type="solid" onPress={this.whereTo}/>
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#E7E7E7',
  },
  logo: {
    width: 150,
    height: 150
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 20,
    letterSpacing: 1.5,
    color: '#ACACAC'
  },
  instructions: {
    textAlign: 'center',
    color: '#333333',
    marginBottom: 5,
  },
  actions: {
    position: 'absolute',
    bottom: '10%'
  },
  // Button Styles
  button: {
    paddingRight: '15%',
    paddingLeft: '15%',
    marginBottom: 0,
    backgroundColor: '#3E8FE5'
  },
  buttonTitleStyle: {
    paddingTop: 10,
    paddingBottom: 10
  }
});
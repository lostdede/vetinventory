import firebase from 'react-native-firebase';
import { VTMongo } from './mongo-service';

/*
 * Database Class which contains methods for interacting with
 * our Firebase DB.
 *
 */
class Database {
  constructor() {
    this.clients = firebase.firestore().collection('clients');
  }

  /* 
   * Retrieve User based on unqiue id.
   * @params id : either an email or company code
   * @params type: either 'code' or 'email'
   * @returns Promise. 
   */
   getClient(id, type = 'code') {
    if (type == 'email') {
      return this.clients.where('email', '==', id).get();
    } else {
      return this.clients.where('id', '==', id).get();
    }
  }

  /*
   * Record user which has signed into application
   * @param {string} ref        - Document Reference
   * @param {array}  userlis    - List of company users 
   */
  companyUserLogin(ref, userList) {
    this.clients.doc(ref).update({users: userList});
    // Handle DB error
  }

  /*
   * Remove user device on sign out
   * @param {string} id        - Company Code
   * @param {string} email     - Company user 
   */
  companyUserLogout(id,email) {
    this.getClient(id)
      .then((query) => {
        this.clients.doc(query.docs[0].id).get().then((doc) => {
          let currentList = doc.data();
          let user = currentList.filter(x => x.email == email)[0];
          currentList.splice(currentList.indexOf(user));
          user.devices = user.devices - 1;
          currentList.push(user);

          this.clients.doc(query.docs[0].id).update({users: currentList});
        });
      })
      // Handle DB error
  }

  /* 
   * Retrieve Inventory based on unqiue user id.
   * @params id : client company code
   * @returns Promise. 
   */
  getInventory(id) {
    let inventoryRef = this.getClient(id)
      .then((query) => {
        let ref = this.clients.doc(query.docs[0].id).collection('inventory');
        return ref;
      });
    return inventoryRef;
  }

  // Check if item is in client DB 
  async clientHasItem(clientid, id) {
    let qV = await this.getClient(clientid).then(queryInvt => queryInvt);

    if (qV.docs) {
      let ref = this.clients.doc(qV.docs[0].id).collection('inventory');

      const hasItem = await ref.where('id', '==', id).get().then(query => {
        if (query.docs.length > 0) {
          return true;
        } else {
          return false;
        }
      });

      return hasItem;
    } else {
      return false;
    }
  }

  /* 
   * Add single item to Inventory based on unqiue user id.
   * @params id : client company code
   * @returns Promise. 
   */
  async addToInventory(client, deviceHolder, barcode, quantity = 1) {
    return this.getClient(client)
      .then((queryInvt) => {
        let ref = this.clients.doc(queryInvt.docs[0].id).collection('inventory');
        ref.where('id', '==', barcode).get().then(async (query) => {
          if (query.docs.length === 0) {
            const product = await VTMongo.doesItemExist(barcode).then(result => {return result});
            ref.add({
              id: barcode,
              quantity: quantity,
              addedBy: deviceHolder,
              addedAt: new Date(),
              updatedAt: new Date(),
              name: product.name !== 'Unknown' ? product.name : null,
              brandName: product.brandName !== 'Unknown' ? product.brandName : null
            });
          } else {
            let currentQuantity = parseInt(query.docs[0].data().quantity);
            ref.doc(query.docs[0].id).update({quantity: currentQuantity + parseInt(quantity), updatedAt:  new Date()});
          }
        });
      });
  }

  async addToPersonalInventory(client, deviceHolder, barcode, quantity = 1, product) {
    return this.getClient(client)
      .then((queryInvt) => {
        let ref = this.clients.doc(queryInvt.docs[0].id).collection('inventory');
        ref.where('id', '==', barcode).get().then(async (query) => {
          if (query.docs.length === 0) {
            ref.add({
              id: barcode,
              quantity: quantity,
              addedBy: deviceHolder,
              addedAt: new Date(),
              updatedAt: new Date(),
              name: product.name !== 'Unknown' ? product.name : null,
              brandName: product.brandName !== 'Unknown' ? product.brandName : null
            });
          } else {
            let currentQuantity = parseInt(query.docs[0].data().quantity);
            ref.doc(query.docs[0].id).update({quantity: currentQuantity + parseInt(quantity), updatedAt:  new Date()});
          }
        });
      });
  }

  /* 
   * Delete single item from Inventory based on unqiue user id.
   * @params id : client company code
   * @returns Promise. 
   */
  deleteFromInventory(client, barcode, quantity = 1) {
    return this.getClient(client)
      .then((queryInvt) => {
        let ref = this.clients.doc(queryInvt.docs[0].id).collection('inventory');
        ref.where('id', '==', barcode).get().then((query) => {
          if (query.docs.length == 0) {
            return;
          } else {
            let currentQuantity = parseInt(query.docs[0].data().quantity) - parseInt(quantity);
            if (currentQuantity === 0) {
              ref.doc(query.docs[0].id).delete();
            } else {
              ref.doc(query.docs[0].id).update({quantity: currentQuantity});
            }
          }
        });
        // Handle DB error
      });
  }
}

const DB = new Database();

export {
  DB 
}
/*
 * VTMongo Class which contains methods for interacting with
 * our MongoDB Server.
 *
 */
import {mongoAPIKey} from './secure';
import { createCipher } from './../lib/crypto';

function getAPIKEY() {
  const d = new Date();
  d.setTime( d.getTime() + d.getTimezoneOffset() * 60 * 1000 );
  const cipher = createCipher('aes-256-ecb', mongoAPIKey);
  return cipher.update(d.toDateString(), 'utf8', 'hex') + cipher.final('hex');
}

class VetInventoryMongo {
  constructor() {
    this.planRoutes = {
      basic: 'https://vt-api.museagram.ca/vetinventory/${key}/product',
      personal: 'https://vt-api.museagram.ca/vetinventory/${key}/product/personal',
    }
  }

  async doesItemExist(barcode, plan = 'basic') {
    const key = getAPIKEY();
    const route = this.planRoutes[plan].replace('${key}', key) + '/does-exist';
    const data = await fetch(route, {
      method: 'POST',
      body: JSON.stringify({
        'id': barcode
      })
    });

    return data;
  }

  async addItem(item, plan = 'basic') {
    const key = getAPIKEY();
    const route = this.planRoutes[plan].replace('${key}', key) + '/insert-one';
    await fetch(route, {
      method: 'POST',
      body: JSON.stringify(item)
    });
  }
}

const VTMongo = new VetInventoryMongo();

export {
  VTMongo
}